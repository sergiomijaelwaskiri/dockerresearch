# Ejemplo Magento2 con Docker

### Repositorio Gitlab

>  https://gitlab.com/sergiomijaelwaskiri/dockerresearch

## Ejecutar docker compose

> `docker-compose up -d`

## Instalar Magento2

> `docker exec -it <container_name> install-magento`

## instalar Contenido de ejemplo

> `docker exec -it <container_name> install-sampledata`

# Links

[https://hub.docker.com/r/alexcheng/magento2](https://hub.docker.com/r/alexcheng/magento2)
