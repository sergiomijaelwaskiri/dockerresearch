# Tutorial de Kubernetes

Kubernetes es un sistema de código libre para la automatización del despliegue, ajuste de escala y manejo de aplicaciones en contenedores​ que fue originalmente diseñado por Google y donado a la Cloud Native Computing Foundation. Soporta diferentes ambientes para la ejecución de contenedores, incluido Docker

# Links

[Kubernetes](https://kubernetes.io/es/)

[Por qué todos apuestan por Kubernetes](https://www.paradigmadigital.com/techbiz/por-que-todos-apuestan-por-kubernetes/)

## videos

[QUÉ ES KUBERNETES](https://www.youtube.com/watch?v=jQsjFBckkDM)

[Openstack, Docker, Kubernetes y Openshift para mi abuela - Alfredo Espejel en T3chFest 2017
](https://www.youtube.com/watch?v=UFE-Nz9cxJg)

[Introduccion a kubernetes](https://youtu.be/e3daDLl4dFk)

[MINIKUBE - Instalá tu cluster de Kubernetes local! / V2M](https://www.youtube.com/watch?v=6e_sXAx7kts)