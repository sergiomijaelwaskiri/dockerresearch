# Comandos docker


## CONTENEDORES

#### Saber que container estan en ejecucion

`docker ps`

#### Saber containes creado mas status

`docker ps -a`

#### ejecutar container (docker ps -a)

> `docker start <id container>`

#### Crear contenedor con un nombre

> `docker run --name <nombre> -it <nombre imagen>`


#### Detener un contenedor

> `docker stop <idContenedor>`

#### Eliminar todos los contenedores de docker

> `docker rm $(docker ps -a -q)`


#### Ejecutar comandos en un container

> `docker run <nombre> <comando>`

Ejemplo ejecutar un bash en un contenedor

`docker run -i -t <nombre> bash`


#### Ingresar a la consola de un contenedor

> `docker attach <id container>`

Salir de un contenedor interactivo sin detenerlo

` Ctrl + p + q`


#### Detener todos los contenedores

> `docker stop $(docker ps -a -q)`


## IMAGENES

#### Descargar imagenes y ejecutar

`docker run hello-word`


#### Listar imagenes

`docker images`

#### Eliminar imagenes

> `docker rmi Image1 Image2`

#### Eliminar todas las imagenes de docker

> `docker rmi $(docker images -q)`


## VOLUMENES

#### Listar volumenes

> `docker volume ls`

#### Eliminar volumen

> `docker volume rm <volumeID>`

#### Eliminar todos los volumenes

> `docker volume ls -qf dangling=true `

Limpieza

> `docker volume rm $(docker volume ls -qf dangling=true) `

O bien, el manejo de un no-op mejor, pero específico de Linux:

> `docker volume ls -qf dangling=true | xargs -r docker volume rm `



